let taskContainer = document.getElementsByClassName("tasks")[0];
let input = document.getElementById("task-input");
let countTasks = document.getElementById("count");
let allTasksButton = document.getElementById("all-tasks");
let activeTasksButton = document.getElementById("active-tasks");
let completedTasksButton = document.getElementById("completed-tasks");
let clearCompletedButton = document.getElementById("clear-completed");
let toggleCheckBox = document.getElementById("toggle-check");

let list = [];
let filter = "all";
let state = false;

window.onload = () => {
  loadFromLocalStorage();
  render(list);
};

input.addEventListener("keydown", (event) => {
  if (event.key == "Enter" && input?.value) {
    if (input.value.length < 2) {
      alert("Task length should be greater than 1");
      return;
    }
    const task = createTask(input.value);
    list.push(task);
    render(list);
  }
});

clearCompletedButton.addEventListener("click", () => {
  list = list.filter((task) => !task.completed);
  render(list);
});

const filterButtons = [
  { button: allTasksButton, filter: "all" },
  { button: activeTasksButton, filter: "active" },
  { button: completedTasksButton, filter: "completed" },
];

function filterTasks(newFilter, filterButton) {
  filter = newFilter;
  setActiveFilterButton(filterButton);
  render(list);
}

filterButtons.forEach(({ button, filter }) => {
  button.addEventListener("click", () => {
    filterTasks(filter, button);
  });
});

toggleCheckBox.addEventListener("click", () => {
  toggleAllTasks();
});

function setActiveFilterButton(activeButton) {
  allTasksButton.classList.remove("active-filter");
  activeTasksButton.classList.remove("active-filter");
  completedTasksButton.classList.remove("active-filter");
  activeButton.classList.add("active-filter");
}

function render(taskList) {
  try {
    taskContainer.innerHTML = "";
    let filteredTasks = taskList.filter((task) => {
      if (filter === "all") return true;
      if (filter === "active") return !task.completed;
      if (filter === "completed") return task.completed;
    });

    filteredTasks.forEach((task) => {
      if (task) {
        let taskNode = task.taskNode;
        let checkbox = taskNode.querySelector(".checkbox");
        checkbox.checked = task.completed;
        const content = taskNode.querySelector(".content");
        if (task.completed) {
          content.classList.add("strikethrough");
        } else {
          content.classList.remove("strikethrough");
        }
        taskContainer.appendChild(taskNode);
      }
    });

    updateCount();
    saveToLocalStorage();
  } catch (error) {
    console.log(error);
    console.log("Error in rendering tasks");
  }
}

function createTask(taskText) {
  try {
    input.value = "";
    let id = Date.now();
    let taskNode = document.createElement("div");
    taskNode.setAttribute("class", "task");

    let content = document.createElement("div");
    content.setAttribute("class", "content");
    content.innerText = taskText;

    content.addEventListener("dblclick", () => {
      let inputField = document.createElement("input");
      inputField.type = "text";
      inputField.value = content.innerText;
      inputField.classList.add("edit-input");
      content.innerText = "";
      content.appendChild(inputField);

      inputField.focus();

      inputField.addEventListener("keydown", (e) => {
        if (e.key === "Enter") {
          saveEdit(id, inputField.value);
        }
      });

      inputField.addEventListener("blur", () => {
        saveEdit(id, inputField.value);
      });
    });

    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.setAttribute("class", "checkbox");
    checkbox.addEventListener("change", () => {
      toggleTaskCompletion(id);
      render(list);
    });

    let deleteTaskButton = document.createElement("i");
    deleteTaskButton.setAttribute("class", "delete-task fa-solid fa-trash");
    deleteTaskButton.addEventListener("click", () => {
      deleteTask(id);
      render(list);
    });

    taskNode.appendChild(checkbox);
    taskNode.appendChild(content);
    taskNode.appendChild(deleteTaskButton);

    return { taskNode, id, completed: false, text: taskText };
  } catch (error) {
    console.log(error);
    console.log("Error creating task");
  }
}

function saveEdit(taskId, newText) {
  list = list.map((task) => {
    if (task.id === taskId) {
      task.text = newText;
      task.taskNode.querySelector(".content").innerText = newText;
    }
    return task;
  });
  render(list);
}

function updateCount() {
  try {
    let activeTasksCount = list.filter((task) => !task.completed).length;
    countTasks.innerText = activeTasksCount;
  } catch (error) {
    console.log(error);
    console.log("Error in updating count");
  }
}

function toggleTaskCompletion(taskId) {
  try {
    list = list.map((task) => {
      if (task.id === taskId) {
        task.completed = !task.completed;
        const content = task.taskNode.querySelector(".content");
        if (task.completed) {
          content.classList.add("strikethrough");
        } else {
          content.classList.remove("strikethrough");
        }
      }
      return task;
    });
    render(list);
  } catch (error) {
    console.log(error);
    console.log("Error toggling task completion");
  }
}

function deleteTask(taskId) {
  try {
    list = list.filter((item) => item.id != taskId);
  } catch (error) {
    console.log(error);
    console.log("Error in deleting task");
  }
}

function toggleAllTasks() {
  try {
    state = !state;
    list.forEach((task) => {
      task.completed = state;
    });
    render(list);
  } catch (error) {
    console.log("Error in changing state of task");
  }
}

function saveToLocalStorage() {
  try {
    const tasksToSave = list.map((task) => ({
      id: task.id,
      text: task.text,
      completed: task.completed,
    }));
    localStorage.setItem("tasks", JSON.stringify(tasksToSave));
  } catch (error) {
    console.log(error);
    console.log("Error saving to local storage");
  }
}

function loadFromLocalStorage() {
  try {
    const savedTasks = JSON.parse(localStorage.getItem("tasks"));
    if (savedTasks) {
      list = savedTasks.map((savedTask) => {
        const task = createTask(savedTask.text);
        task.id = savedTask.id;
        task.completed = savedTask.completed;
        return task;
      });
    }
  } catch (error) {
    console.log(error);
    console.log("Error loading from local storage");
  }
}
