# [Todo App]('https://todo-app-drill.netlify.app/')

A simple and interactive Todo App to help manage tasks efficiently. This app allows you to add, edit, delete, and filter tasks based on their status (all, active, completed). Additionally, you can clear all completed tasks and toggle the completion status of all tasks.

## Features

- Add new tasks
- Edit existing tasks
- Delete tasks
- Mark tasks as completed or active
- Filter tasks by all, active, or completed
- Clear all completed tasks
- Toggle the completion status of all tasks
- Custom scrollbar styling
- Strike-through text for completed tasks

## Technologies Used

- HTML
- CSS
- JavaScript


## Screenshot

![]('./../images/Screenshot.png)


![]('./../images/Screenshot-mobile-view.png)
